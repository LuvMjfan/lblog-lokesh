
import os
import sys

basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))


WIN = sys.platform.startswith('win')
prefix = 'sqlite:///'


class BaseConfig(object):
    SECRET_KEY = os.getenv('SECRET_KEY', 'dev key')

    DEBUG_TB_INTERCEPT_REDIRECTS = False

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True

    CKEDITOR_ENABLE_CSRF = True
    CKEDITOR_FILE_UPLOADER = 'admin.upload_image'

    MAIL_SERVER = os.getenv('SMTP')
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = os.getenv('lokesh0kashyap@gmail.com')
    MAIL_PASSWORD = os.getenv('lokesh0076')
    MAIL_DEFAULT_SENDER = ('LBLOG Admin', MAIL_USERNAME)

    LBLOG_EMAIL = os.getenv('LBLOG_EMAIL')
    LBLOG_POST_PER_PAGE = 10
    LBLOG_MANAGE_POST_PER_PAGE = 15
    LBLOG_COMMENT_PER_PAGE = 15
    LBLOG_THEMES = {'perfect_blue': 'Perfect Blue', 'black_swan': 'Black Swan'}
    LBLOG_SLOW_QUERY_THRESHOLD = 1

    LBLOG_UPLOAD_PATH = os.path.join(basedir, 'uploads')
    LBLOG_ALLOWED_IMAGE_EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif']



class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL', prefix + os.path.join(basedir, 'lokeshblog.db'))


config = {

    'production': ProductionConfig
}
